public class L implements I 
{
	public static void main(String[] args) 
	{
		private int a;
		private M m;
		
		public M getM() 
		{
			return m;
		}

		public void setM(M m) 
		{
			this.m=m;
		}

		public void b() 
		{}

		public int getA() 
		{
			return a;
		}

		public void setA(int a) 
		{
			this.a=a;
		}

		public void i() 
		{}
	}
}

class M 
{
	private A a;

	public A getA() 
	{
		return a;
	}
	public void setA(A a) 
	{
		this.a=a;
	}
}

class A 
{}

class B 
{}

interface I 
{
	public void i();
}

class X
{
	public void met(L I)
	{}
}