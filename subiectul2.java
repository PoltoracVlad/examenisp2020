import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class subiectul2 extends JFrame
{
	JLabel label1, label2, label3;
	JTextField text1, text2, text3;
    JButton buton;
    
    subiectul2()
    {
    	setTitle("Suma numarului de caractere");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
        setResizable(false);
    }
	
    public void init()
	{
		this.setLayout(null);
        int latime=250;
        int inaltime=30;
        label1=new JLabel("Text 1");
        label1.setBounds(10, 50, latime, inaltime);
        text1=new JTextField();
        text1.setBounds(100, 50, latime, inaltime);
        label2=new JLabel("Text 2");
        label2.setBounds(10, 100, latime, inaltime);
        text2=new JTextField();
        text2.setBounds(100, 100, latime, inaltime);
        label3=new JLabel("Suma");
        label3.setBounds(10, 150, latime, inaltime);
        text3=new JTextField();
        text3.setBounds(100, 150, latime, inaltime);
        buton=new JButton("Aduna");
        buton.setBounds(10, 200, latime, inaltime);
        buton.addActionListener(new Adunare());
        add(label1);
        add(text1);
        add(label2);
        add(text2);
        add(label3);
        add(text3);
        add(buton);
	}
    
	public static void main(String[] args) 
	{
		new subiectul2();
	}
	
	class Adunare implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
        {
			String t1=text1.getText();
	        int s1=t1.length();
	        String t2=text2.getText();
	        int s2=t2.length();
	        int s3=s1+s2;
	        text3.setText(String.valueOf(s3));
        }
	}
}
